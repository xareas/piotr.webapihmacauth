﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Piotr.WebApiHmacAuth.Web.Infrastructure
{
    public class HmacSigningHandler : HttpClientHandler
    {
        private readonly ISecretRepository _secretRepository;
        private readonly IBuildMessageRepresentation _representationBuilder;
        private readonly ICalculteSignature _signatureCalculator;

        public string Username { get; set; }

        public HmacSigningHandler(ISecretRepository secretRepository,
                              IBuildMessageRepresentation representationBuilder,
                              ICalculteSignature signatureCalculator)
        {
            _secretRepository = secretRepository;
            _representationBuilder = representationBuilder;
            _signatureCalculator = signatureCalculator;
        }

        protected  override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
                                                                System.Threading.CancellationToken cancellationToken)
        {
            if (!request.Headers.Contains(Configuration.UsernameHeader))
            {
                request.Headers.Add(Configuration.UsernameHeader, Username);
            }
            request.Headers.Date = new DateTimeOffset(DateTime.Now,DateTime.Now-DateTime.UtcNow);
            var representation = _representationBuilder.BuildRequestRepresentation(request);
            var secret = _secretRepository.GetSecretForUser(Username);
            string signature = _signatureCalculator.Signature(secret,
                representation);

            var header = new AuthenticationHeaderValue(Configuration.AuthenticationScheme, signature);

            request.Headers.Authorization = header;
            return base.SendAsync(request, cancellationToken);
        }
    }
}