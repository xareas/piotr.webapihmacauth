﻿using System.Net.Http;

namespace Piotr.WebApiHmacAuth.Web.Infrastructure
{
    public interface IBuildMessageRepresentation
    {
        string BuildRequestRepresentation(HttpRequestMessage requestMessage);
    }
}