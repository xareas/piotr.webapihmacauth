﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Piotr.WebApiHmacAuth.Web.Infrastructure;

namespace Piotr.WebApiHmacAuth.Web.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IQueryable<string> Get()
        {
            return new string[] { "value1", "value2" }.AsQueryable();
        }

        // GET api/values/5
        public async Task<string> Get(int id)
        {
            var signingHandler = new HmacSigningHandler(new DummySecretRepository(), new CanonicalRepresentationBuilder(),
                                                    new HmacSignatureCalculator());
            signingHandler.Username = "username";

            var client = new HttpClient(new RequestContentMd5Handler()
                                            {
                                                InnerHandler = signingHandler
                                            });
            await client.GetAsync("http://localhost:48564/api/values");

            return "value";

        }

        // POST api/values
        public string Post([FromBody]string value)
        {
            return value;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}