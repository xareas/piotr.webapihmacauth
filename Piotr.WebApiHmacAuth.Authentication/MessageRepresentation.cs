﻿using System;

namespace Piotr.WebApiHmacAuth.Web.Infrastructure
{
    public class MessageRepresentation
    {
        public string Representation { get; set; }
        public string Username { get; set; }
        public DateTime Date { get; set; }
    }
}