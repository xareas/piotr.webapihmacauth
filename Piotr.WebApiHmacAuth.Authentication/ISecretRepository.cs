﻿namespace Piotr.WebApiHmacAuth.Web.Infrastructure
{
    public interface ISecretRepository
    {
        string GetSecretForUser(string username);
    }
}