﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Piotr.WebApiHmacAuth.Web.Infrastructure
{
    public class DummySecretRepository : ISecretRepository
    {
        private readonly IDictionary<string, string> _userPasswords
            = new Dictionary<string, string>()
                  {
                      {"username","password"}
                  };

        public string GetSecretForUser(string username)
        {
            if (!_userPasswords.ContainsKey(username))
            {
                return null;
            }

            var userPassword = _userPasswords[username];
            var hashed = ComputeHash(userPassword, new SHA1CryptoServiceProvider());
            return hashed;
        }

        private string ComputeHash(string inputData, HashAlgorithm algorithm)
        {
            byte[] inputBytes = Encoding.UTF8.GetBytes(inputData);
            byte[] hashed = algorithm.ComputeHash(inputBytes);
            return Convert.ToBase64String(hashed);
        }
    }
}